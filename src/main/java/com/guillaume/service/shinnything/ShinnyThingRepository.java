package com.guillaume.service.shinnything;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

import javax.enterprise.context.ApplicationScoped;
import java.util.UUID;
@ApplicationScoped

public class ShinnyThingRepository implements PanacheRepositoryBase<ShinnyThing, UUID> {
}
