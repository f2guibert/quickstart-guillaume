package com.guillaume.service.shinnything;


import io.quarkus.hibernate.orm.rest.data.panache.PanacheRepositoryResource;
import io.quarkus.rest.data.panache.ResourceProperties;

import java.util.UUID;

@ResourceProperties(path = "/api/shinny-thing")
public interface ShinnyThingResource extends PanacheRepositoryResource<ShinnyThingRepository, ShinnyThing, UUID> {

}
