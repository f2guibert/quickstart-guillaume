package com.guillaume.service.shinnything;

import com.guillaume.service.buyer.Buyer;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "shinny_thing")
public class ShinnyThing {
    @Id
    @Column
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator")
    private UUID uuid;

    @Column(name = "model")
    private String model;

    @Column(name = "price")
    private String price;

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
            name = "shinny_thing_buyer",
            joinColumns = {@JoinColumn(name = "shinny_thing_uuid")},
            inverseJoinColumns = {@JoinColumn(name = "buyer_uuid")}
    )
    private List<Buyer> buyers = new ArrayList<>();

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public List<Buyer> getBuyers() {
        return buyers;
    }

    public void setBuyers(List<Buyer> buyers) {
        this.buyers = buyers;
    }
}
