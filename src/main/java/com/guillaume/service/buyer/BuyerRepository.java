package com.guillaume.service.buyer;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

import javax.enterprise.context.ApplicationScoped;
import java.util.UUID;

@ApplicationScoped
public class BuyerRepository implements PanacheRepositoryBase<Buyer, UUID> {
}
