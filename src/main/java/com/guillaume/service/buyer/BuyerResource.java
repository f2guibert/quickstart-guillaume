package com.guillaume.service.buyer;

import io.quarkus.hibernate.orm.rest.data.panache.PanacheRepositoryResource;
import io.quarkus.rest.data.panache.ResourceProperties;

import java.util.UUID;

@ResourceProperties(path = "/api/buyer")
public interface BuyerResource extends PanacheRepositoryResource<BuyerRepository, Buyer, UUID> {

}
